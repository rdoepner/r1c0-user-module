# User module for Zend Framework 2 #
------------------------------------

This is a user module based on Doctrine 2 for [Zend Framwork 2](http://framework.zend.com/).

## Usage ##

Generate salt and hash values:

```
#!php

$salt = Zend\Math\Rand::getString(32, null, true);

$bcrypt = new Zend\Crypt\Password\Bcrypt(array(
    'salt' => $salt
));

$hash = $bcrypt->create('my-secret-phrase');
```