<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0UserModule\Controller;

use R1c0UserModule\Options\ModuleOptions;
use R1c0UserModule\Service\UserServiceInterface;
use R1c0UserModule\Form\Login as LoginForm;
use R1c0BaseModule\Controller\BaseActionController;
use Zend\View\Model\ViewModel;
use Zend\Validator\AbstractValidator;
use Zend\I18n\Translator\TranslatorInterface;

class IndexController extends BaseActionController
{

    const ROUTE_LOGIN = "user/login";

    const ROUTE_LOGOUT = "user/logout";

    const MESSAGE_LOGIN_SUCCESS = "messageLoginSuccess";

    const MESSAGE_LOGOUT_SUCCESS = "messageLogoutSuccess";

    const MESSAGE_LOGIN_FAILED = "messageLoginFailed";

    protected $messageTemplates = array(
        self::MESSAGE_LOGIN_SUCCESS => "You have successfully logged in.",
        self::MESSAGE_LOGOUT_SUCCESS => "You have successfully logged out.",
        self::MESSAGE_LOGIN_FAILED => "Your login has failed."
    );

    protected $moduleOptions;

    protected $userService;

    protected $loginForm;

    protected $translator;

    public function __construct(ModuleOptions $moduleOptions, UserServiceInterface $userService, LoginForm $loginForm, TranslatorInterface $translator)
    {
        $this->setModuleOptions($moduleOptions);
        $this->setUserService($userService);
        $this->setLoginForm($loginForm);
        $this->setTranslator($translator);
    }

    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;
        
        return $this;
    }

    public function getModuleOptions()
    {
        return $this->moduleOptions;
    }

    public function setUserService(UserServiceInterface $userService)
    {
        $this->userService = $userService;
        
        return $this;
    }

    public function getUserService()
    {
        return $this->userService;
    }

    public function setLoginForm($loginForm)
    {
        $this->loginForm = $loginForm;
        
        return $this;
    }

    public function getLoginForm()
    {
        return $this->loginForm;
    }

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        
        return $this;
    }

    public function getTranslator()
    {
        return $this->translator;
    }

    public function indexAction()
    {
        if (! $user = $this->identity()) {
            return $this->redirect()->toRoute(self::ROUTE_LOGIN);
        }
        
        return new ViewModel(array(
            'user' => $user
        ));
    }

    public function loginAction()
    {
        if ($this->identity() !== null) {
            $routes = $this->getModuleOptions()->getRedirectRoutes();
            
            return $this->redirect()->toRoute($routes['login']);
        }
        
        $loginForm = $this->getLoginForm();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            AbstractValidator::setDefaultTranslatorTextDomain('r1c0/user');
            
            $loginForm->setData($request->getPost());
            
            if ($loginForm->isValid()) {
                $messenger = $this->flashMessenger();
                $validated = $loginForm->getData();
                $userService = $this->getUserService();
                
                if ($userService->login($validated['email'], $validated['password'])) {
                    $messenger->setNamespace('success');
                    $messenger->addMessage($this->getTranslator()
                        ->translate($this->messageTemplates[self::MESSAGE_LOGIN_SUCCESS], 'r1c0/user'));
                    
                    $routes = $this->getModuleOptions()->getRedirectRoutes();
                    
                    return $this->redirect()->toRoute($routes['login']);
                }
                
                $messenger->setNamespace('error');
                $messenger->addMessage($this->getTranslator()
                    ->translate($this->messageTemplates[self::MESSAGE_LOGIN_FAILED], 'r1c0/user'));
                
                return $this->redirect()->refresh();
            }
        }
        
        return new ViewModel(array(
            'elementErrors' => array_keys($loginForm->getMessages()),
            'isPost' => $request->isPost(),
            'form' => $loginForm
        ));
    }

    public function logoutAction()
    {
        $routes = $this->getModuleOptions()->getRedirectRoutes();
        $userService = $this->getUserService();
        $userService->logout();
        
        $messenger = $this->flashMessenger();
        $messenger->setNamespace('success');
        $messenger->addMessage($this->getTranslator()
            ->translate($this->messageTemplates[self::MESSAGE_LOGOUT_SUCCESS], 'r1c0/user'));
        
        return $this->redirect()->toRoute($routes['logout']);
    }
}
