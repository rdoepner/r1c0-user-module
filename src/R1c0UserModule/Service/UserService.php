<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0UserModule\Service;

use R1c0UserModule\Service\UserServiceInterface;
use R1c0BaseModule\Service\AbstractService;
use Zend\Authentication\AuthenticationService;

class UserService extends AbstractService implements UserServiceInterface
{

    const EVENT_LOGIN_PRE = "login.pre";

    const EVENT_LOGIN_POST = "login.post";

    const EVENT_LOGOUT_PRE = "logout.pre";

    const EVENT_LOGOUT_POST = "logout.post";

    protected $authService;

    public function __construct(AuthenticationService $authService)
    {
        $this->setAuthService($authService);
    }

    public function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
        
        return $this;
    }

    public function getAuthService()
    {
        return $this->authService;
    }

    public function login($email, $password)
    {
        $authService = $this->getAuthService();
        $authAdapter = $authService->getAdapter();
        $authAdapter->setIdentityValue($email);
        $authAdapter->setCredentialValue($password);
        
        $this->getEventManager()->trigger(self::EVENT_LOGIN_PRE, $this, array(
            'email' => $email,
            'password' => $password
        ));
        
        $authResult = $authService->authenticate();
        
        if ($authResult->isValid()) {
            $this->getEventManager()->trigger(self::EVENT_LOGIN_POST, $this, array(
                'user' => $authResult->getIdentity()
            ));
            
            return true;
        }
        
        return false;
    }

    public function logout()
    {
        $authService = $this->getAuthService();
        
        if ($authService->hasIdentity()) {
            $this->getEventManager()->trigger(self::EVENT_LOGOUT_PRE, $this, array(
                'user' => $authService->getIdentity()
            ));
            
            $authService = $this->getAuthService();
            $authService->clearIdentity();
            
            $this->getEventManager()->trigger(self::EVENT_LOGOUT_POST, $this);
        }
    }
}
