<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0UserModule\Service;

use R1c0UserModule\Service\AclServiceInterface;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Acl;

class AclService implements AclServiceInterface
{

    protected $acl;

    public function __construct(array $aclService = array())
    {
        if (array_key_exists('roles', $aclService)) {
            if (is_array($aclService['roles'])) {
                $this->addRoles($aclService['roles']);
            }
        }
        
        if (array_key_exists('resources', $aclService)) {
            if (is_array($aclService['resources'])) {
                $this->addResources($aclService['resources']);
            }
        }
        
        if (array_key_exists('allow', $aclService)) {
            if (is_array($aclService['allow'])) {
                $this->addAllow($aclService['allow']);
            }
        }
    }

    public function addRoles(array $roles)
    {
        $acl = $this->getAcl();
        
        foreach ($roles as $role) {
            if (! $acl->hasRole($role)) {
                $acl->addRole(new GenericRole($role));
            }
        }
    }

    public function addResources(array $resources)
    {
        $acl = $this->getAcl();
        
        foreach ($resources as $resource) {
            if (! $acl->hasResource($resource)) {
                $acl->addResource(new GenericResource($resource));
            }
        }
    }

    public function addAllow(array $allow)
    {
        $acl = $this->getAcl();
        
        foreach ($allow as $role => $resources) {
            if ($acl->hasRole($role)) {
                $roleObject = $acl->getRole($role);
                
                if ($resources !== null) {
                    if (is_array($resources)) {
                        foreach ($resources as $resource => $privileges) {
                            if ($acl->hasResource($resource)) {
                                $resourceObject = $acl->getResource($resource);
                                
                                if ($privileges !== null) {
                                    $acl->allow($roleObject, $resourceObject, $privileges);
                                } else {
                                    $acl->allow($roleObject, $resourceObject);
                                }
                            }
                        }
                    }
                } else {
                    $acl->allow($roleObject);
                }
            }
        }
    }

    public function setAcl($acl)
    {
        $this->acl = $acl;
        
        return $this;
    }

    public function getAcl()
    {
        if (! $this->acl instanceof Acl) {
            $this->acl = new Acl();
        }
        
        return $this->acl;
    }
}
