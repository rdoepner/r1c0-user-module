<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0UserModule\Entity;

use R1c0UserModule\Entity\UserRole;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminator_class", type="string")
 * @ORM\Table(name="r1c0_user")
 */
class User
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="display_name", type="string", length=50)
     */
    protected $displayName;

    /**
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="hash", type="string", length=60)
     */
    protected $hash;

    /**
     * @ORM\Column(name="salt", type="string", length=32)
     */
    protected $salt;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active = 1;

    /**
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * @ORM\ManyToOne(targetEntity="UserRole", inversedBy="users")
     */
    protected $role;

    public function getId()
    {
        return $this->id;
    }

    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        
        return $this;
    }

    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
        
        return $this;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setCreated(DateTime $created)
    {
        $this->created = $created;
        
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setRole(UserRole $role)
    {
        $this->role = $role;
        
        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }
}
