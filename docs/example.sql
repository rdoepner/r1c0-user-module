-- -----------------------------------------------------
-- Table `r1c0_user_example`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_user_example`;

CREATE TABLE IF NOT EXISTS `r1c0_user_example` (
  `id` INT UNSIGNED NOT NULL,
  `firstname` VARCHAR(50) NOT NULL,
  `lastname` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
    FOREIGN KEY (`id`)
    REFERENCES `r1c0_user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;
