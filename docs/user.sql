-- -----------------------------------------------------
-- Table `r1c0_user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_user_role`;

CREATE TABLE IF NOT EXISTS `r1c0_user_role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(25) NOT NULL,
  `label` VARCHAR(50) NOT NULL,
  `position` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table data `r1c0_user_role`
-- -----------------------------------------------------
INSERT INTO `r1c0_user_role` (`id`, `name`, `label`, `position`) VALUES
(1, 'administrator', 'Administrator', 1),
(2, 'registered', 'Registered User', 2),
(3, 'publisher', 'Publisher', 3),
(4, 'editor', 'Editor', 4);

-- -----------------------------------------------------
-- Table `r1c0_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_user`;

CREATE TABLE IF NOT EXISTS `r1c0_user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` INT UNSIGNED NOT NULL,
  `discriminator_class` VARCHAR(50) NOT NULL DEFAULT 'user',
  `display_name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(100) UNIQUE NOT NULL,
  `hash` CHAR(60) NOT NULL,
  `salt` CHAR(32) NOT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
    UNIQUE KEY (`email`),
    FOREIGN KEY (`role_id`)
    REFERENCES `r1c0_user_role` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;
