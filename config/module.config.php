<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
return array(
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        'controller' => 'R1c0UserModule\Controller\IndexController',
                        'action' => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'login' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'controller' => 'R1c0UserModule\Controller\IndexController',
                                'action' => 'login'
                            )
                        )
                    ),
                    'logout' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'controller' => 'R1c0UserModule\Controller\IndexController',
                                'action' => 'logout'
                            )
                        )
                    )
                )
            )
        )
    ),
    'controllers' => array(
        'factories' => array(
            'R1c0UserModule\Controller\IndexController' => 'R1c0UserModule\Controller\IndexControllerFactory'
        )
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'R1c0UserModule\Controller\Plugin\Acl' => 'R1c0UserModule\Controller\Plugin\Acl'
        ),
        'aliases' => array(
            'Acl' => 'R1c0UserModule\Controller\Plugin\Acl'
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'R1c0UserModule\Service\UserService' => 'R1c0UserModule\Service\UserServiceFactory',
            'R1c0UserModule\Service\AclService' => 'R1c0UserModule\Service\AclServiceFactory'
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'R1c0UserModule\View\Helper\Acl' => 'R1c0UserModule\View\Helper\Acl'
        ),
        'aliases' => array(
            'Acl' => 'R1c0UserModule\View\Helper\Acl'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'user_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/R1c0UserModule/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'R1c0UserModule\Entity' => 'user_annotation_driver'
                )
            )
        ),
        'authentication' => array(
            'orm_default' => array(
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'R1c0UserModule\Entity\User',
                'identity_property' => 'email',
                'credential_property' => 'hash',
                'credential_callable' => function (R1c0UserModule\Entity\User $user, $password) {
                    
                    if ($user->getActive()) {
                        $bcrypt = new Zend\Crypt\Password\Bcrypt(array(
                            'salt' => $user->getSalt()
                        ));
                        
                        return $bcrypt->verify($password, $user->getHash());
                    }
                    
                    return false;
                }
            )
        )
    )
);
